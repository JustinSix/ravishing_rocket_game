﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{

    [SerializeField]float rcsThrust = 250f;
    [SerializeField]float mainThrust = 1000f;
    [SerializeField]float levelLoadDelay = 2f;

    [SerializeField]AudioClip mainEngine;
    [SerializeField]AudioClip death;
    [SerializeField]AudioClip success;

    [SerializeField]ParticleSystem mainEngineParticles;
    [SerializeField]ParticleSystem deathParticles;
    [SerializeField]ParticleSystem successParticles;

    Rigidbody rigidBody;
    AudioSource audioSource;

    enum State { Alive, Dying, Transcending};
    State state = State.Alive;
    private int correctLevel;
    bool collisionsDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(state == State.Alive)
        {
        RespondToThrustInput();
        RespondToRotateInput();
        }
        if (Debug.isDebugBuild)
        {
        RespondToDebugKeys();
        }
    }
    
    void OnCollisionEnter(Collision collision)
    {
        if(state != State.Alive || collisionsDisabled) { return; }; 
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartSuccessSequence()
    {
        state = State.Transcending;
        audioSource.Stop();
        audioSource.PlayOneShot(success);
        successParticles.Play();
        Invoke("LoadNextScene", levelLoadDelay);
    }

    private void StartDeathSequence()
    {
        state = State.Dying;
        audioSource.Stop();
        audioSource.PlayOneShot(death);
        deathParticles.Play();
        Invoke("LoadCurrentLevel", levelLoadDelay);
    }

    private void LoadNextScene()
    {
        correctLevel = SceneManager.GetActiveScene().buildIndex + 1;
        if (correctLevel == SceneManager.sceneCountInBuildSettings)
        {
            correctLevel = 0;
        }
        SceneManager.LoadScene(correctLevel); 

    }

    private void LoadCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();

        }
        else
        {
            audioSource.Stop();
            mainEngineParticles.Stop();
        }
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
        mainEngineParticles.Play();
    }

    private void RespondToRotateInput()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RotateManually(rcsThrust * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RotateManually(-rcsThrust * Time.deltaTime);
        }

    }

    private void RotateManually(float rotationThisFrame)
    {
        rigidBody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame);
        rigidBody.freezeRotation = false;
    }

    private void RespondToDebugKeys()
    {

        if (Input.GetKey(KeyCode.L))
        {
            LoadNextScene();
        }
        else if(Input.GetKey(KeyCode.C)) 
        {
            collisionsDisabled = !collisionsDisabled; // toggle
        }

    }

}
